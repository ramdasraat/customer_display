#include <flutter/dart_project.h>
#include <flutter/flutter_view_controller.h>
#include <windows.h>

#include "flutter_window.h"
#include "run_loop.h"
#include "utils.h"

BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData) {
    static int monitorIndex = 0;
    monitorIndex++;
    if (monitorIndex == 2) {
        HMONITOR* secondMonitor = reinterpret_cast<HMONITOR*>(dwData);
        *secondMonitor = hMonitor;
        return FALSE; // Stop enumerating
    }
    return TRUE; // Continue enumeration
}

int APIENTRY wWinMain(_In_ HINSTANCE instance, _In_opt_ HINSTANCE prev,
                      _In_ wchar_t *command_line, _In_ int show_command) {
    // Attach to console when present (e.g., 'flutter run') or create a
    // new console when running with a debugger.
    if (!::AttachConsole(ATTACH_PARENT_PROCESS) && ::IsDebuggerPresent()) {
        CreateAndAttachConsole();
    }

    // Initialize COM, so that it is available for use in the library and/or plugins.
    ::CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED);

    RunLoop run_loop;

    flutter::DartProject project(L"data");

    std::vector<std::string> command_line_arguments = GetCommandLineArguments();
    project.set_dart_entrypoint_arguments(std::move(command_line_arguments));

    FlutterWindow window(&run_loop, project);
    Win32Window::Point origin(10, 10);
    Win32Window::Size size(1280, 720);
    if (!window.CreateAndShow(L"Customer Display", origin, size)) {
        return EXIT_FAILURE;
    }
    window.SetQuitOnClose(true);

    int displayCount = GetSystemMetrics(SM_CMONITORS);
    if (displayCount > 1) {
        HWND targetWindow = window.GetHandle();
        if (targetWindow != NULL) {
            HMONITOR secondMonitor = NULL;
            EnumDisplayMonitors(NULL, NULL, MonitorEnumProc, reinterpret_cast<LPARAM>(&secondMonitor));

            if (secondMonitor != NULL) {
                MONITORINFO monitorInfo = { sizeof(monitorInfo) };
                if (GetMonitorInfo(secondMonitor, &monitorInfo)) {
                    int targetX = monitorInfo.rcMonitor.left;
                    int targetY = monitorInfo.rcMonitor.top;
                    int targetWidth = monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left;
                    int targetHeight = monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top;

                    // Move the window to the second monitor
                    SetWindowPos(targetWindow, HWND_TOP, targetX, targetY, targetWidth, targetHeight, SWP_NOZORDER | SWP_SHOWWINDOW);
                }
            }
        }
    }

    run_loop.Run();

    ::CoUninitialize();
    return EXIT_SUCCESS;
}