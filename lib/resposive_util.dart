import 'package:flutter/material.dart';

class ResponsiveTextUtils {
  // Base screen dimensions for reference (assuming a typical POS display)
  static const double _baseWidth = 1280.0;

  // Get scaling factor based on current screen size
  static double _getScaleFactor(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final double width = size.width;
    
    // Calculate diagonal size for more accurate scaling
    
    // Use different approach based on orientation
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      return width / _baseWidth;
    } else {
      // For landscape, adjust to ensure text isn't too large
      final double scaleFactor = (width / _baseWidth) * 0.8;
      // Limit scaling to reasonable bounds
      return scaleFactor.clamp(0.5, 1.5);
    }
  }

  // Get responsive font size based on base size and current screen
  static double getResponsiveFontSize(BuildContext context, double baseSize) {
    final double scaleFactor = _getScaleFactor(context);
    // Apply scaling with limits to prevent extreme sizes
    final double scaledSize = baseSize * scaleFactor;
    
    // Set reasonable min/max boundaries
    final double minSize = baseSize * 0.7;
    final double maxSize = baseSize * 1.3;
    
    return scaledSize.clamp(minSize, maxSize);
  }

  // Font size presets for common text elements
  static double heading1Size(BuildContext context) => 
      getResponsiveFontSize(context, 28.0);
  
  static double heading2Size(BuildContext context) => 
      getResponsiveFontSize(context, 22.0);
  
  static double normalTextSize(BuildContext context) => 
      getResponsiveFontSize(context, 16.0);
  
  static double smallTextSize(BuildContext context) => 
      getResponsiveFontSize(context, 12.0);
  
  static double totalTextSize(BuildContext context) => 
      getResponsiveFontSize(context, 20.0);
  
  static double cartItemTextSize(BuildContext context) => 
      getResponsiveFontSize(context, 14.0);
}

// Extension on TextStyle for easier use
extension ResponsiveTextStyleExtension on TextStyle {
  TextStyle responsive(BuildContext context, {double? baseFontSize}) {
    final double fontSize = baseFontSize ?? this.fontSize ?? 16.0;
    return copyWith(
      fontSize: ResponsiveTextUtils.getResponsiveFontSize(context, fontSize)
    );
  }
}

// Extension on Text widgets for one-liner usage
extension ResponsiveTextExtension on Text {
  Text responsive(BuildContext context, {double? baseFontSize}) {
    final TextStyle? currentStyle = this.style;
    final double baseFontSizeValue = baseFontSize ?? 
        currentStyle?.fontSize ?? 
        14.0;
    
    return Text(
      this.data ?? '',
      style: (currentStyle ?? const TextStyle())
          .copyWith(fontSize: ResponsiveTextUtils.getResponsiveFontSize(
              context, baseFontSizeValue)),
      key: this.key,
      strutStyle: this.strutStyle,
      textAlign: this.textAlign,
      textDirection: this.textDirection,
      locale: this.locale,
      softWrap: this.softWrap,
      overflow: this.overflow,
      textScaler: this.textScaler,
      maxLines: this.maxLines,
      semanticsLabel: this.semanticsLabel,
      textWidthBasis: this.textWidthBasis,
      textHeightBehavior: this.textHeightBehavior,
      selectionColor: this.selectionColor,
    );
  }
}