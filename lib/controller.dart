import 'package:get/get.dart';

class Customer extends GetxController {
  var images = [].obs;

  addImages(var data) {
    images.addAll(List<String>.from(data.map((x) => x['image'].toString())));
    update();
  }

  clear()
  {
    images.clear();
    update();
  }
}
