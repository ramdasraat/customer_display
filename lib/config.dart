import 'package:flutter/material.dart';

enum Flavor {
  SIPO,
  TAKEY,
  DEV,
  LOGPOS,
}

class Config {
  static late Flavor appFlavor;

  static String get name {
    switch (appFlavor) {
      case Flavor.SIPO:
        return 'Sipo';
      case Flavor.LOGPOS:
        return 'Silver Back';
      case Flavor.TAKEY:
      default:
        return 'LogPos';
    }
  }

  static String get admin {
    switch (appFlavor) {
      case Flavor.SIPO:
        return 'https://sisov2.sipocloudpos.com';
      case Flavor.LOGPOS:
        return 'https://adminsilverbackcloudpos.com';
      case Flavor.TAKEY:
      default:
        return 'https://sipocloudpos.com/admin';
    }
  }

  static String get sipoNode {
    switch (appFlavor) {
      case Flavor.SIPO:
        return "https://node.sipocloudpos.com/";
      case Flavor.DEV:
        return "https://node.develop.sipocloudpos.com/";
      case Flavor.TAKEY:
        return "https://node.takey.co.nz/";
      case Flavor.LOGPOS:
        return "https://silverbackcloudpos.com/sipo_server_live/public/api/";
      }
  }

  static String get eventServer {
    switch (appFlavor) {
      case Flavor.SIPO:
        return 'https://sisov2.sipocloudpos.com';
      case Flavor.DEV:
        return 'https://siso.develop.sipocloudpos.com';
      case Flavor.LOGPOS:
        return 'https://siso.silverbackcloudpos.com';
      case Flavor.TAKEY:
      return 'https://sipocloudpos.com/admin';
    }
  }

  static String get endPoint {
    switch (appFlavor) {
      case Flavor.SIPO:
        return "https://adminapi.sipocloudpos.com/public/api";
      case Flavor.LOGPOS:
        return "https://silverbackcloudpos.com/sipo_server_live/public/api";
      case Flavor.TAKEY:
        return "https://www.takey.co.nz/sipo-backend/public/api";
      case Flavor.DEV:
        return "https://dev.sipocloudpos.com/dev_server/public/api";
      }
  }

  static String get logo {
    switch (appFlavor) {
      case Flavor.SIPO:
        return "assets/images/sipologo2@4x.png";
      case Flavor.LOGPOS:
        return "assets/images/SILVERBACK-LOGO.png";
      default:
        return "assets/images/sipologo2@4x.png";
    }
  }

  static String get logosquare {
    switch (appFlavor) {
      case Flavor.SIPO:
        return "assets/images/sipologo.png";
      case Flavor.LOGPOS:
        return "assets/images/icon1.png";
      default:
        return "assets/images/sipologo.png";
    }
  }

  static Color get primaryColor {
    switch (appFlavor) {
      case Flavor.SIPO:
        return Color.fromRGBO(254, 191, 1, 1);
      case Flavor.LOGPOS:
        return Color.fromRGBO(29, 206, 199, 1);
      default:
        return Color.fromRGBO(254, 191, 1, 1);
    }
  }

  static Color get OnlineprimaryColor {
    switch (appFlavor) {
      case Flavor.SIPO:
        return Color(0xFFF9AC00);
      case Flavor.LOGPOS:
        return Color.fromRGBO(29, 206, 199, 1);
      default:
        return Color(0xFFF9AC00);
    }
  }

  static Color get OnlineprimaryColorDark {
    switch (appFlavor) {
      case Flavor.SIPO:
        return Color(0xFFFD1E26);
      case Flavor.LOGPOS:
        return Color.fromARGB(255, 3, 138, 133);
      default:
        return Color(0xFFFD1E26);
    }
  }

  static Color get primaryColorDark {
    switch (appFlavor) {
      case Flavor.SIPO:
        return Color.fromRGBO(255, 129, 0, 1);
      case Flavor.LOGPOS:
        return Color.fromRGBO(40, 160, 134, 1);
      default:
        return Color.fromRGBO(255, 129, 0, 1);
    }
  }

  static Image get logoFormatted {
    switch (appFlavor) {
      case Flavor.SIPO:
        return Image.asset(
          Config.logo,
          fit: BoxFit.contain,
          //height: 70,
          width: 50,
        );
      case Flavor.LOGPOS:
        return Image.asset(
          Config.logosquare,
          fit: BoxFit.contain,
          height: double.maxFinite,
          //width: 50,
        );
      default:
        return Image.asset(
          Config.logo,
          fit: BoxFit.contain,
          //height: 70,
          width: 50,
        );
    }
  }

  static String get backgroud {
    switch (appFlavor) {
      case Flavor.SIPO:
        return "assets/images/Group.png";
      case Flavor.LOGPOS:
        return "assets/images/GroupSilver.png";
      default:
        return "assets/images/Group.png";
    }
  }

  static String get currency {
    switch (appFlavor) {
      case Flavor.SIPO:
        return "\$";
      case Flavor.LOGPOS:
        return "₹";
      default:
        return "\$";
    }
  }

  static String get appname {
    switch (appFlavor) {
      case Flavor.SIPO:
        return "Sipo";
      case Flavor.LOGPOS:
        return "Silver back";
      default:
        return "Sipo";
    }
  }

  static Icon get appIcon {
    switch (appFlavor) {
      case Flavor.SIPO:
        return new Icon(Icons.new_releases);
      case Flavor.TAKEY:
      default:
        return new Icon(Icons.developer_mode);
    }
  }
}
