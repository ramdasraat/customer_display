import 'dart:async';
import 'dart:convert';
import 'dart:ffi';
import 'dart:io';
import 'dart:math';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:customer_display/OrderModel.dart';
import 'package:customer_display/client_message_event.dart';
import 'package:customer_display/config.dart';
import 'package:customer_display/ip_dialog.dart';
import 'package:customer_display/payment_screen.dart';
import 'package:customer_display/resposive_util.dart';
import 'package:dio/dio.dart' as dio;
import 'package:ffi/ffi.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:system_tray/system_tray.dart' as syst;
import 'package:wakelock_plus/wakelock_plus.dart';
import 'package:web_socket_client/web_socket_client.dart';
import 'package:win32/win32.dart';
import 'package:window_manager/window_manager.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'controller.dart';

class ProductScreenCustomer extends StatefulWidget {
  const ProductScreenCustomer({Key? key}) : super(key: key);

  @override
  _ProductScreenCustomerState createState() => _ProductScreenCustomerState();
}

class _ProductScreenCustomerState extends State<ProductScreenCustomer>
    with TickerProviderStateMixin {
  final RxList<TicketObject> unordered = RxList<TicketObject>();
  CustomerObject? customer;
  final ScrollController _scrollController = ScrollController();

  int? staffId;
  int? guestCount;
  int? tableId;

  bool? mergeOrder;
  bool? autoModifier;
  int productColumns = 5;
  int modifierColumns = 5;

  bool productView = true;
  bool cartView = false;
  bool modifierView = false;

  double animatedSize = 25;
  bool loading = false;
  bool categoryType = false;
  bool paymentReady = false;
  String type = '';
  String crypto = '';

  bool? forceLogin;

  var selectedPriceTag;
  var selectedPriceTagName = "Price Tag";
  var selectedPriceTagObject;

  double paid = 0;
  String id = "123";
  bool turnOnCode = true;

  String footerstrip = "Welcome ";
  String? status;

  String cus_name = "";
  String cus_id = "";
  String staff_name = "";
  String discount = "0.00";
  double itemDiscount = 0;
  String surcharge = "0.00";
  String gratuity = "0.00";
  late EventChannel _stream;
  double weight = 0;
  String message = "";
  int lastTap = DateTime.now().millisecondsSinceEpoch;
  int consecutiveTaps = 0;
  bool logo = true;

  static const platform =
      MethodChannel('com.outlogics.customerDisplay/DISPLAY');
  final box = GetStorage("CustomerDisplay");

  var _shopLogo = "";
  String res_id = "";
  final Rx<double> _weight = 0.0.obs;
  double _price = 0;
  WebSocket? socket;
  final syst.SystemTray _systemTray = syst.SystemTray();
  final syst.AppWindow _appWindow = syst.AppWindow();
  Map<String, Map<String, dynamic>> promoSummaryData = {};

  // Cached calculations
  String _cachedTotalSum = "0.00";
  String _cachedTax = "0.00";
  double _cachedWholeTotal = 0.0;
  bool _needsRecalculation = true;

  @override
  void initState() {
    super.initState();
    if (!kIsWeb) {
      if (Platform.isAndroid) {
        _stream = EventChannel('scaleReadEvent');
        _stream.receiveBroadcastStream().listen((onData) {
          setState(() {
            weight = double.parse(onData.toString()) / 1000;
          });
        });
      }
    }
    initSocket();
    Future.delayed(const Duration(seconds: 5), () {
      turnOnCode = false;
      setState(() {});
    });

    _weight.listen(
      (p0) {
        if (p0 == 0) {
          while (Get.isDialogOpen ?? false) {
            Get.close(1);
          }
        } else {
          while (Get.isDialogOpen ?? false) {
            Get.close(1);
          }
          Get.dialog(WeightDialog(p0, _price));
        }
      },
    );
    if (Platform.isWindows) {
      initSystemTray();
    }

    // Listen for changes to unordered list to trigger recalculation
    unordered.listen((_) {
      _needsRecalculation = true;
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    socket?.close();
    super.dispose();
  }

  // Perform calculations only when needed
  void _updateCalculations() {
    if (_needsRecalculation) {
      _cachedTotalSum = _calculateTotalSum();
      _cachedTax = _calculateTax();
      _cachedWholeTotal = _calculateWholeTotal();
      _needsRecalculation = false;
    }
  }

  String getTotalSum() {
    _updateCalculations();
    return _cachedTotalSum;
  }

  String _calculateTotalSum() {
    double sum = 0;
    itemDiscount = 0;
    for (final f in unordered) {
      if (f.voidItem == 1) {
        continue;
      }
      sum += f.get();
      sum -= (f.rateOfDiscount! * f.get() / 100);
      itemDiscount += (f.rateOfDiscount! * f.get() / 100);
      sum += (f.notePrice ?? 0.0);
      for (final mod in f.modifiers!) {
        sum += (mod.price! * mod.quantity!);
      }
    }
    return sum.toStringAsFixed(2);
  }

  String getTax() {
    _updateCalculations();
    return _cachedTax;
  }

  String _calculateTax() {
    var tax = double.parse(_cachedTotalSum) * (3 / 23);
    return tax.toStringAsFixed(2);
  }

  double wholeTotal() {
    _updateCalculations();
    return _cachedWholeTotal;
  }

  double _calculateWholeTotal() {
    return double.parse(_cachedTotalSum) +
        double.parse(gratuity) +
        double.parse(surcharge) -
        double.parse(discount);
  }

  String getTotalQuantity() {
    double sum = 0;
    for (final f in unordered) {
      sum += f.count!;
    }
    return sum.toStringAsFixed(0);
  }

  double getProductCount(id) {
    try {
      var object = unordered.where((test) => test.menu!.id == id);
      return object.isEmpty
          ? 0
          : object.fold(0.0, (sum, obj) => sum + obj.count!);
    } catch (e) {
      return 0;
    }
  }

  Widget _buildCartItem(
      TicketObject order, int index, Orientation orientation) {
    return OrderList(
      key: Key('order_$index'),
      order: order,
      index: index,
      selectedCount: order.count?.toInt() ?? 0,
    );
  }

  Widget _buildCartList(Orientation orientation) {
    if (unordered.isEmpty) {
      return Center(
        child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage("assets/images/Group.png"),
            ),
          ),
          child: const Center(
            child: Text("Cart is empty"),
          ),
        ),
      );
    }

    // Schedule scroll to bottom after items are loaded
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (unordered.isNotEmpty && _scrollController.hasClients) {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut,
        );
      }
    });

    return ListView.separated(
      separatorBuilder: (context, index) => const Divider(),
      itemCount: unordered.length,
      controller: _scrollController,
      itemBuilder: (context, index) =>
          _buildCartItem(unordered[index], index, orientation),
    );
  }

  Widget _buildLogoWidget() {
    if (_shopLogo.isEmpty) {
      return Image(image: AssetImage(Config.logo), fit: BoxFit.contain);
    } else {
      return CachedNetworkImage(
        imageUrl: _shopLogo,
        fit: BoxFit.contain,
        placeholder: (context, url) =>
            const Center(child: CircularProgressIndicator()),
        errorWidget: (context, url, error) =>
            Image(image: AssetImage(Config.logo)),
      );
    }
  }

  Widget _buildCarouselImages(Customer controller) {
    return Obx(() {
      return CarouselSlider(
        options: CarouselOptions(
          autoPlay: true,
          viewportFraction: 1,
          disableCenter: true,
          autoPlayInterval: const Duration(seconds: 3),
          autoPlayAnimationDuration: const Duration(milliseconds: 800),
          autoPlayCurve: Curves.fastOutSlowIn,
        ),
        items: controller.images.map((imageUrl) {
          return CachedNetworkImage(
            imageUrl: imageUrl,
            width: double.infinity,
            height: MediaQuery.of(context).size.height,
            fit: BoxFit.cover,
            placeholder: (context, url) => Container(color: Colors.grey[300]),
            errorWidget: (context, url, error) => Container(),
          );
        }).toList(),
      );
    });
  }

  Widget _buildEmptyCartView(bool isPotrait) {
    return Column(
      children: [
        Expanded(
          flex: 14,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Stack(
              children: [
                Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 100,
                        width: 200,
                        child: InkWell(
                          onTap: () {
                            int now = DateTime.now().millisecondsSinceEpoch;
                            if (now - lastTap < 300) {
                              consecutiveTaps++;
                              if (consecutiveTaps > 1) {
                                setState(() {
                                  logo = !logo;
                                });
                              }
                            } else {
                              consecutiveTaps = 0;
                            }
                            lastTap = now;
                          },
                          onLongPress: () async {
                            var ip = TextEditingController();
                            var address = box.read<String?>("ADDRESS");
                            if (address != null && address.isNotEmpty) {
                              ip.text = address;
                            }
                            await Get.dialog(IpDialog(id, ip));
                          },
                          child: Visibility(
                            visible: logo && !isPotrait,
                            child: _buildLogoWidget(),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: Container(
            width: double.infinity,
            color: Colors.black54,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  footerstrip,
                  style: const TextStyle(fontSize: 20),
                ),
                const SizedBox(height: 10),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _buildPromoSummary() {
    if (promoSummaryData.isEmpty) return const SizedBox.shrink();

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Applied Promotions',
                style: TextStyle(
                  color: Colors.black87,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ).responsive(context),
              ),
            ],
          ),
          ...promoSummaryData.entries.map((entry) {
            String promoName = entry.key;
            double totalSavings = entry.value['totalSavings'] as double;
            int itemCount = entry.value['itemCount'] as int;
            int setCount = entry.value['setCount'] as int;

            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      setCount > 1
                          ? '${setCount}x $promoName ($itemCount items)'
                          : '$promoName ($itemCount items)',
                      style: const TextStyle(color: Colors.black87)
                          .responsive(context),
                    ),
                  ),
                  Text(
                    'Saved: \$${totalSavings.toStringAsFixed(2)}',
                    style: const TextStyle(
                      color: Colors.green,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            );
          }).toList(),
        ],
      ),
    );
  }

  Widget _buildTotalsSummary(BuildContext context, Orientation orientation) {
    final TextStyle regularStyle = TextStyle(
      color: Colors.black,
      fontFamily: 'Akkurat',
      fontSize: 14,
    ).responsive(context);

    final TextStyle boldStyle = TextStyle(
      color: Colors.black,
      fontFamily: 'Akkurat',
      fontWeight: FontWeight.bold,
      fontSize: orientation != Orientation.portrait ? 16 : 22,
    ).responsive(context);

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Tax (Includes)", style: regularStyle),
              Text("${Config.currency}${getTax()}", style: regularStyle),
            ],
          ),
          if (discount != "0.00")
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Discount", style: regularStyle),
                Text("-${Config.currency}$discount", style: regularStyle),
              ],
            ),
          if (itemDiscount != 0)
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Item Discount", style: regularStyle),
                Text("-${Config.currency}${itemDiscount.toStringAsFixed(2)}",
                    style: regularStyle),
              ],
            ),
          if (surcharge != "0.00")
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Surcharge", style: regularStyle),
                Text("${Config.currency}$surcharge", style: regularStyle),
              ],
            ),
          if (gratuity != "0.00")
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Gratuity", style: regularStyle),
                Text("${Config.currency}$gratuity", style: regularStyle),
              ],
            ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Sub Total", style: regularStyle),
              Text("${Config.currency}${getTotalSum()}", style: regularStyle),
            ],
          ),
          const SizedBox(height: 4),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Total", style: boldStyle),
              Text(
                "${Config.currency}${wholeTotal().toStringAsFixed(2)}",
                style: boldStyle,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildCartView(Orientation orientation) {
    return Container(
      color: const Color(0xffffffff),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Center(
              child: Text(
                'Checkout',
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ).responsive(context),
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(width: .5, color: Colors.black12),
                  bottom: BorderSide(width: .5, color: Colors.black12),
                ),
              ),
              child: _buildCartList(orientation),
            ),
          ),
          if (promoSummaryData.isNotEmpty) _buildPromoSummary(),
          const SizedBox(height: 5),
          _buildTotalsSummary(context, orientation),
          const Divider(color: Colors.black12, height: 1),
        ],
      ),
    );
  }

  Widget _buildContentWithCart(Orientation orientation) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        if (orientation != Orientation.portrait)
          Expanded(
            flex: 50,
            child: Center(
              child: SizedBox(
                height: 100,
                width: 200,
                child: _buildLogoWidget(),
              ),
            ),
          ),
        if (unordered.isNotEmpty)
          Expanded(
            flex: 50,
            child: _buildCartView(orientation),
          ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final bool isPotrait =
        MediaQuery.of(context).orientation == Orientation.portrait;

    return OrientationBuilder(
      builder: (context, orientation) {
        if (loading) {
          return const Center(child: CircularProgressIndicator());
        }

        return Scaffold(
          body: GetBuilder<Customer>(builder: (controller) {
            return Stack(
              children: [
                // Background/Main content
                InkWell(
                  onDoubleTap: () async {
                    try {
                      if (await windowManager.isFullScreen()) {
                        await windowManager.setFullScreen(false);
                      } else {
                        await windowManager.setFullScreen(true);
                      }
                    } catch (_) {}
                  },
                  child: SizedBox(
                    width: double.infinity,
                    height: double.infinity,
                    child: isPotrait
                        ? _buildLogoWidget()
                        : _buildCarouselImages(controller),
                  ),
                ),

                // Payment screen overlay
                if (paymentReady)
                  PaymentScreen(
                    balance: message,
                    cryptoQr: crypto,
                    status: status,
                    subTotal: double.parse(getTotalSum()),
                    total: wholeTotal(),
                    tax: double.tryParse(getTax()) ?? 0,
                    type: type,
                    gratuity: gratuity,
                    surcharge: surcharge,
                    discount: double.tryParse(discount) ?? 0,
                    itemDiscount: itemDiscount,
                  ),

                // Main content when not in payment mode
                if (!paymentReady)
                  SizedBox.expand(
                    child: unordered.isEmpty
                        ? _buildEmptyCartView(isPotrait)
                        : _buildContentWithCart(orientation),
                  ),
              ],
            );
          }),
        );
      },
    );
  }

  void fireSync(eventData) {
    if (eventData.isEmpty) {
      setState(() {
        status = null;
        paymentReady = false;
        type = '';
      });
    }
    var orders =
        List<TicketObject>.from(eventData.map((x) => TicketObject.fromJson(x)));
    if (orders.isEmpty) {
      unordered.clear();
      discount = "0.00";
      surcharge = "0.00";
      gratuity = "0.00";
    } else {
      if (res_id.isEmpty || res_id != getStoreId(orders.first)) {
        res_id = orders.first.menu?.userId.toString() ?? "";
        print("writing terminal id $res_id");
        box.write("RES_ID", res_id);
        getImages();
      }
      unordered.clear();
      for (var p in orders) {
        if (p.comboBatch != null) {
          var comboFilterlist =
              orders.where((p0) => p0.comboBatch == p.comboBatch);
          var comboCalculatedrate = 0.0;
          for (var combo in comboFilterlist) {
            comboCalculatedrate +=
                ((combo.tempRate ?? 0) * (combo.count ?? 0)) -
                    ((combo.rateOfDiscount ?? 0) *
                        ((combo.tempRate ?? 0) * (combo.count ?? 0) / 100));
          }
          p.comboCombinedPrice = comboCalculatedrate;
        }
      }
      unordered.addAll(orders);
    }
    setState(() {
      _needsRecalculation = true;
      promoSummaryData = calculatePromoSummary(orders);
    });
  }

  String? getStoreId(TicketObject? object) {
    if (object?.store_id != null) {
      return object?.store_id.toString();
    } else if (object?.menu?.userId != null) {
      return object?.menu?.userId.toString();
    }
    return "";
  }

  void getImages() async {
    if (res_id.isEmpty) {
      return;
    }
    await Future.delayed(const Duration(seconds: 2));
    try {
      print("getting images ${res_id}");
      dio.Response image = await dio.Dio()
          .get("${Config.sipoNode}customer/getcustomerdisplayimage/${res_id}")
          .catchError((onError) {
        Get.find<Customer>().clear();
        return dio.Response(
          requestOptions: dio.RequestOptions(
            path:
                "${Config.sipoNode}customer/getcustomerdisplayimage/${res_id}",
          ),
          statusCode: 500,
        );
      });
      Get.find<Customer>()
          .addImages(image.data["datas"]["customerdisplayImages"]);
      var restaurant = image.data['datas']['restaurant'];
      var meta = image.data['datas']['meta'];
      cus_name = restaurant['name'].toString();
      print(meta?['footer_information']);
      footerstrip = meta?['footer_information'].toString() ?? "";
      _shopLogo = restaurant['shop_logo'] ?? "";
      setState(() {});
    } catch (e) {
      Get.find<Customer>().clear();
    }
  }

  void closeAlert() {
    Get.close(1);
  }

  void initSocket() async {
    String? idFromCustomerDisplay = await getIdFromPlatformChannel();
    id = idFromCustomerDisplay ?? (await box.read<String>("KEY")) ?? "";
    if (id.isEmpty) {
      var rng = Random();
      id = rng.nextInt(10000000).toString();
      box.write("KEY", id);
    } else {
      box.write("KEY", id);
    }
    Get.defaultDialog(
        middleText: id,
        middleTextStyle: const TextStyle(fontSize: 40, color: Colors.black));
    Future.delayed(const Duration(seconds: 5), () {
      if (Get.isDialogOpen == true) {
        Get.close(1);
      }
    });
    res_id = await box.read<String>("RES_ID") ?? "";
    if (Platform.isAndroid) {
      WakelockPlus.enable();
    }
    getImages();
    setState(() {});
    loadWebSocketChannel();
  }

  void customerDisplayEvent(eventData) {
    status = null;

    if (eventData['status'] != null) {
      if (eventData['type'] != null) {
        setState(() {
          message = '';
          status = eventData['status'];
          paymentReady = true;
          print("Payment ready $paymentReady");
          type = eventData['type'];
        });
      }
      return;
    }

    if (eventData['alert'] != null) {
      if (eventData['type'] != null) {
        setState(() {
          message = eventData['alert'];
          paymentReady = true;
          print("Payment ready $paymentReady");
          type = eventData['type'];
        });
      }
      return;
    }

    if (eventData['image_added'] != null) {
      message = '';
      getImages();
      return;
    }

    if (eventData['Weight'] != null) {
      setState(() {
        _price = double.tryParse(eventData['price'].toString()) ?? 0;
        _weight.value = double.tryParse(eventData['Weight'].toString()) ?? 0;
      });
      return;
    }

    if (eventData['PAY_IT_NOW_QR_CLOSE'] != null) {
      setState(() {
        paymentReady = false;
        type = '';
      });
      return;
    }

    if (eventData['PAY_IT_NOW_QR'] != null) {
      setState(() {
        paymentReady = true;
        type = 'crypto';
        crypto = eventData['PAY_IT_NOW_QR'].toString();
      });
      return;
    }

    if (eventData['clear'] != null) {
      unordered.clear();
      discount = "0.00";
      surcharge = "0.00";
      gratuity = "0.00";
      setState(() {
        paymentReady = false;
        _needsRecalculation = true;
      });
      return;
    }

    fireSync(eventData['orders']);
    if (eventData['discount'] != null) {
      setState(() {
        discount = eventData['discount'].toString();
        _needsRecalculation = true;
      });
    }

    if (eventData['surcharge'] != null) {
      setState(() {
        surcharge = eventData['surcharge'].toString();
        _needsRecalculation = true;
      });
    }

    if (eventData['gratuity'] != null) {
      setState(() {
        gratuity = eventData['gratuity'].toString();
        _needsRecalculation = true;
      });
    }

    if (eventData['staff'] != null) {
      setState(() {
        staff_name = eventData['staff'].toString();
      });
    }
  }

  Future<String?> getIdFromPlatformChannel() async {
    try {
      final String? result = await platform.invokeMethod('SECOND_DISPLAY_CODE');
      return result;
    } catch (e) {
      return null;
    }
  }

  Future<String?> localSyncUrl() async {
    try {
      final String? result = await platform.invokeMethod('LOCAL_SYNC_URL');
      return result;
    } catch (e) {
      return null;
    }
  }

  void loadWebSocketChannel() async {
    const backoff = ConstantBackoff(Duration(seconds: 1));
    var url = 'wss://websocket.sipocloudpos.com/app/wgkkiktmo38aajp22vvl';
    socket = WebSocket(
      Uri.parse(url),
      pingInterval: const Duration(seconds: 1),
      backoff: backoff,
    );
    socket?.connection.listen((state) {
      print(state);
      print(id);
      if (state is Connected) {
        _emitSocketChannel(socket, id);
      } else if (state is Reconnected) {
        _emitSocketChannel(socket, id);
      }
    });

    socket?.messages.listen((message) {
      try {
        Map<String, dynamic> incommingMessage = jsonDecode(message);
        if (incommingMessage['event'] == 'pusher:ping') {
          sendMessage(
            ClientMessageEvent(
              event: "pusher:pong",
              data: null,
              room: null,
              owner: null,
            ),
          );
        } else {
          if (!incommingMessage.containsKey("event")) {
            customerDisplayEvent(incommingMessage);
          }
        }
      } catch (e) {
        print(e);
      }
    });
  }

  void sendMessage(ClientMessageEvent message) {
    socket?.send(jsonEncode(message.toJson()));
  }

  void _emitSocketChannel(WebSocket? socket, String id) {
    socket?.send(
      jsonEncode({
        "event": "pusher:subscribe",
        "data": {"channel": id},
      }),
    );
  }

  Future<void> initSystemTray() async {
    await _systemTray.initSystemTray(
      title: "system tray",
      iconPath: 'assets/sipo_icon.ico',
    );

    // create context menu
    final syst.Menu menu = syst.Menu();
    await menu.buildFrom([
      syst.MenuItemLabel(
          label: "$id",
          onClicked: (menuItem) {
            Clipboard.setData(ClipboardData(text: "$id"));
          }),
      syst.MenuItemLabel(
          label: 'Show', onClicked: (menuItem) => _appWindow.show()),
      syst.MenuItemLabel(
          label: 'Hide', onClicked: (menuItem) => _appWindow.hide()),
      syst.MenuItemLabel(
          label: 'Exit',
          onClicked: (menuItem) {
            exit(0);
          }),
    ]);

    // set context menu
    await _systemTray.setContextMenu(menu);

    // handle system tray event
    _systemTray.registerSystemTrayEventHandler((eventName) {
      debugPrint("eventName: $eventName");
      if (eventName == syst.kSystemTrayEventClick) {
        Platform.isWindows ? _appWindow.show() : _systemTray.popUpContextMenu();
      } else if (eventName == syst.kSystemTrayEventRightClick) {
        Platform.isWindows ? _systemTray.popUpContextMenu() : _appWindow.show();
      }
    });
    await windowManager.setFullScreen(true);
  }
}

// Optimized OrderList widget
class OrderList extends StatelessWidget {
  const OrderList({
    Key? key,
    this.canAdd = true,
    this.canRemove = true,
    this.canModify = true,
    this.selected = false,
    this.selectedCount = 0,
    required this.order,
    this.index = 0,
  }) : super(key: key);

  final bool canAdd;
  final bool canRemove;
  final bool canModify;
  final bool selected;
  final TicketObject order;
  final int selectedCount;
  final int index;

  @override
  Widget build(BuildContext context) {
    String? comboBatch = index == 0 ? "" : null;

    var displayComboName = false;
    if (order.comboBatch != null &&
        order.comboBatch!.isNotEmpty &&
        comboBatch != order.comboBatch &&
        order.promo_name == null) {
      displayComboName = true;
      comboBatch = order.comboBatch;
    }

    final bool isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;

    return Row(
      children: [
        const SizedBox(width: 5),
        // Product image for landscape orientation
        if (order.menu?.photo != null &&
            (order.menu?.photo?.isNotEmpty ?? false) &&
            !isPortrait)
          SizedBox(
            width: 40,
            height: 40,
            child: CachedNetworkImage(
              imageUrl: order.menu!.photo!,
              fit: BoxFit.cover,
              placeholder: (context, url) => Container(color: Colors.grey[200]),
              errorWidget: (context, url, error) =>
                  Container(color: Colors.grey[300]),
            ),
          ),
        if ((order.menu?.photo == null ||
                (order.menu?.photo?.isEmpty ?? false)) &&
            !isPortrait)
          const SizedBox(width: 40),

        Expanded(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // Combo name if applicable
              if (displayComboName)
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Row(
                      children: [
                        Flexible(
                          child: Text(
                            order.combo_name ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 20,
                              fontStyle: FontStyle.italic,
                              color: Colors.black,
                            ).responsive(context),
                          ),
                        ),
                        const SizedBox(width: 10),
                        Text(
                          (order.comboCombinedPrice ?? 0.0).toStringAsFixed(2),
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 25,
                            fontStyle: FontStyle.italic,
                          ).responsive(context),
                        ),
                      ],
                    ),
                  ),
                ),

              // Item detail row
              Row(
                children: <Widget>[
                  const SizedBox(width: 10),
                  Expanded(
                    flex: 60,
                    child: Row(
                      children: <Widget>[
                        // Show quantity if applicable
                        if (selectedCount > 0)
                          Text(
                            "$selectedCount x ",
                            style: TextStyle(
                              fontSize: !isPortrait ? 14 : 12,
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ).responsive(context),
                          ),
                        // Item name and price
                        Expanded(
                          child: Text(
                            '${order.menu?.name} - ${!isPortrait ? _priceModule(order) : ''}',
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            maxLines: 3,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: !isPortrait ? 14 : 12,
                              fontWeight: FontWeight.w600,
                              decoration: order.voidItem == 0
                                  ? TextDecoration.none
                                  : TextDecoration.lineThrough,
                              decorationThickness: 2,
                              decorationColor: Colors.red,
                            ).responsive(context),
                          ),
                        ),
                      ],
                    ),
                  ),

                  // Show individual price if not part of combo
                  if (order.comboBatch == null ||
                      order.comboBatch!.isEmpty ||
                      order.promo_name != null)
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          "\$" + order.get().toStringAsFixed(2),
                          style: TextStyle(
                            color: Colors.black,
                          ).responsive(context),
                        ),
                        const SizedBox(width: 15),
                      ],
                    ),

                  // Show void icon if applicable
                  if (order.voidItem == 1)
                    Icon(
                      Icons.close,
                      color: Colors.red.shade900,
                      size: 35,
                    ),
                ],
              ),

              // Size info if available
              if (order.size != null)
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 30, 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        order.size ?? '',
                        style: TextStyle(
                          color: Colors.black,
                        ).responsive(context),
                      ),
                      const Text(''),
                    ],
                  ),
                ),

              // Discount info if applicable
              if (order.rateOfDiscount! > 0 && order.comboBatch == null)
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 5, 5, 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        width: 25,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white),
                        child: Text(
                          "Discount",
                          style: TextStyle(
                            color: Colors.red,
                            fontSize: !isPortrait ? 14 : 12,
                          ),
                        ).responsive(context),
                      ),
                      Spacer(),
                      Text(
                        "- \$${(order.rateOfDiscount! * order.tempRate! * order.count! / 100).toStringAsFixed(2)}  ",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: !isPortrait ? 14 : 12,
                        ).responsive(context),
                      ),
                    ],
                  ),
                ),

              // Void reason if item is voided
              if (order.voidItem == 1)
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 20,
                        child: Text(
                          "",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                          ).responsive(context),
                        ),
                      ),
                      Expanded(
                        flex: 60,
                        child: Text(
                          order.voidReason ?? "",
                          style: TextStyle(
                            color: Colors.black,
                          ).responsive(context),
                        ),
                      ),
                      Expanded(
                        flex: 20,
                        child: Text(
                          "",
                          style: TextStyle(
                            color: Colors.black,
                          ).responsive(context),
                        ),
                      ),
                    ],
                  ),
                ),

              // Kitchen note if available
              if (order.kitchenNote != null)
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 5, 15, 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        width: 25,
                      ),
                      Text(
                        order.kitchenNote ?? "",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: !isPortrait ? 14 : 12,
                        ).responsive(context),
                      ),
                      Spacer(),
                      Text(
                        "\$" + (order.notePrice ?? 0.0).toStringAsFixed(2),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: !isPortrait ? 14 : 12,
                        ).responsive(context),
                      ),
                    ],
                  ),
                ),

              // Tag info if available
              if (order.tag != null && order.tag is Map)
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(
                      order.tag['tag_name'],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: !isPortrait ? 10 : 14,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ).responsive(context),
                    ),
                  ),
                ),

              // Modifiers list
              Column(
                children: List.generate(order.modifiers!.length, (index) {
                  return Padding(
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 5),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          "${order.modifiers![index].name} x ${order.modifiers![index].quantity}",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: !isPortrait ? 14 : 12,
                          ).responsive(context),
                        ),
                        Spacer(),
                        Text(
                          "${Config.currency}${order.modifiers![index].price! * order.modifiers![index].quantity!}",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: !isPortrait ? 14 : 12,
                          ).responsive(context),
                        ),
                      ],
                    ),
                  );
                }),
              ),
            ],
          ),
        ),
      ],
    );
  }

  String _priceModule(TicketObject order) {
    if (order.comboBatch != null &&
        order.comboBatch!.isNotEmpty &&
        order.promoId == null) {
      return "";
    }
    return "\$" + (order.tempRate?.toStringAsFixed(2) ?? "0");
  }
}

// Weight dialog with performance optimizations
class WeightDialog extends StatelessWidget {
  final double weight;
  final double price;

  const WeightDialog(this.weight, this.price, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.white54,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          color: Colors.white12,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "${weight.toStringAsFixed(3)} Kg",
                  style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ),
              ),
              if (price != 0)
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "\$${price.toStringAsFixed(2)}",
                    style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}

extension HexColor on Color {
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${(a * 255).round().toRadixString(16).padLeft(2, '0')}'
      '${r.round().toRadixString(16).padLeft(2, '0')}'
      '${g.round().toRadixString(16).padLeft(2, '0')}'
      '${b.round().toRadixString(16).padLeft(2, '0')}';
}

Map<String, Map<String, dynamic>> calculatePromoSummary(
    List<TicketObject> orders) {
  Map<String?, Map<String?, List<TicketObject>>> promosByBatch = {};
  Map<String?, String?> promos = {};

  for (var order in orders) {
    if (order.promoId != null) {
      promosByBatch.putIfAbsent(order.promoId, () => {});
      var batchGroup = promosByBatch[order.promoId]!;
      batchGroup.putIfAbsent(order.comboBatch, () => []).add(order);
      promos[order.promoId] = order.promo_name;
    }
  }

  Map<String, Map<String, dynamic>> promoSummary = {};

  promosByBatch.forEach((promoId, batchGroups) {
    if (promoId != null) {
      var promoName = promos[promoId] ?? "";

      int setCount = batchGroups.length;
      int totalItems = 0;
      double totalSavings = 0.0;

      batchGroups.forEach((comboBatch, orders) {
        totalItems += orders.length;
        double batchSavings = orders.fold(0.0, (sum, order) {
          double orderCount = order.count ?? 0.0;
          double tempRate = order.tempRate ?? 0.0;
          double rateOfDiscount = order.rateOfDiscount ?? 0.0;
          return sum + (rateOfDiscount * (tempRate * orderCount) / 100);
        });
        totalSavings += batchSavings;
      });

      promoSummary[promoName] = {
        'totalSavings': totalSavings,
        'itemCount': totalItems,
        'setCount': setCount,
      };
    }
  });

  return promoSummary;
}

Future<void> moveToSecondScreen(BuildContext context) async {
  final hwnd = GetForegroundWindow();
  final hMonitor =
      MonitorFromWindow(hwnd, MONITOR_FROM_FLAGS.MONITOR_DEFAULTTONEAREST);
  final monitorInfoPtr = calloc<MONITORINFO>();
  monitorInfoPtr.ref.cbSize = sizeOf<MONITORINFO>();
  GetMonitorInfo(hMonitor, monitorInfoPtr);
  SetWindowPos(
    hwnd,
    HWND_TOP,
    monitorInfoPtr.ref.rcMonitor.left,
    monitorInfoPtr.ref.rcMonitor.top,
    0,
    0,
    SET_WINDOW_POS_FLAGS.SWP_NOSIZE | SET_WINDOW_POS_FLAGS.SWP_NOZORDER,
  );
  calloc.free(monitorInfoPtr);
}
