import 'dart:async';
import 'dart:convert';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/status.dart' as status;

class WebSocketManager {
  final String url;
  late WebSocketChannel _channel;
  final Duration reconnectInterval;
  StreamController<Map<String, dynamic>> _streamController =
      StreamController.broadcast();
  bool _shouldReconnect = true;
  int _reconnectAttempts = 0;
  final int _maxReconnectAttempts = 5;

  WebSocketManager({
    required this.url,
    this.reconnectInterval = const Duration(seconds: 5),
  });

  Stream<Map<String, dynamic>> get stream => _streamController.stream;

  void connect() {
    print("Got connect request");
    print(url);
    try {
      _channel = WebSocketChannel.connect(Uri.parse(url));
    } catch (e) {
      print(e);
    }
    _channel.stream.listen(
      (message) {
        print("got data");
        print(message);
        // Handle incoming messages
        final data = jsonDecode(message);
        _streamController.add(data);
      },
      onDone: () {
        print("onDone");
        if (_shouldReconnect) {
          print("reconnection Triggered");
          _reconnect();
        }
      },
      onError: (error) {
        print("On connection error");
        print(error);
        if (_shouldReconnect) {
          print("reconnection Triggered");
          _reconnect();
        }
      },
    );
  }

  void _reconnect() {
    if (_reconnectAttempts < _maxReconnectAttempts) {
      _reconnectAttempts++;
      Future.delayed(_reconnectIntervalWithBackoff(), () {
        if (_shouldReconnect) {
          print("Attempting to reconnect, attempt #$_reconnectAttempts");
          connect();
        }
      });
    } else {
      print("Max reconnect attempts reached. Stopping reconnect.");
      _shouldReconnect = false;
    }
  }

  Duration _reconnectIntervalWithBackoff() {
    // Exponential backoff strategy
    return Duration(
        seconds: reconnectInterval.inSeconds * (1 << (_reconnectAttempts - 1)));
  }

  void send(Map<String, dynamic> message) {
    _channel.sink.add(jsonEncode(message));
  }

  void close() {
    _shouldReconnect = false;
    _channel.sink.close(status.goingAway);
  }

  void dispose() {
    _streamController.close();
    close();
  }
}
