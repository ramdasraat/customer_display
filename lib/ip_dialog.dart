import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class IpDialog extends StatefulWidget {
  final TextEditingController ip;
  final String id;
  const IpDialog(this.id, this.ip, {Key? key}) : super(key: key);
  @override
  State<IpDialog> createState() => _IpDialogState();
}
class _IpDialogState extends State<IpDialog> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: InkWell(
        onTap: () {
          Clipboard.setData(ClipboardData(text: widget.id));
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                widget.id,
                style: const TextStyle(
                  fontSize: 30,
                  color: Colors.black),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

