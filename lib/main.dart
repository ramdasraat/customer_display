import 'dart:io';

import 'package:customer_display/ProductScreen.dart';
import 'package:customer_display/config.dart';
import 'package:customer_display/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:window_manager/window_manager.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init("CustomerDisplay");
  if (Platform.isWindows) {
     await windowManager.ensureInitialized();
    WindowOptions windowOptions = WindowOptions(
      skipTaskbar: true,
    );

    // Set up the window and initialize
    windowManager.waitUntilReadyToShow(windowOptions, () async {
      await windowManager.show();
      await windowManager.setPreventClose(
          true); // Optional: prevent closing from the 'X' button
    });
  }
  runApp(MyApp(Flavor.SIPO));
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();

  MyApp(Flavor config) {
    Get.put(Customer());
    Config.appFlavor = config;
  }
}

class _MyAppState extends State<MyApp> {

  bool fullscreen = false;

  //   // if you use socket communication and TCP.
  // final Cambel.Communicator<IO.Socket, Cambel.SocketConnectionPoint> communicator = Cambel.Tcp();
  // late final Cambel.Camel<IO.Socket, Cambel.SocketConnectionPoint> receiver;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Customer Display',
      theme: ThemeData(
        fontFamily: 'Akkurat',
        textTheme: TextTheme(
          bodyLarge: TextStyle(color: Colors.white),
          bodyMedium: TextStyle(color: Colors.white),
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      builder: (context, child) {
        return Stack(
          children: [
            child!,
          ],
        );
      },
      home: ProductScreenCustomer(),
    );
  }
}
