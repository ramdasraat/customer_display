import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'ProductScreen.dart';

class Login extends StatefulWidget {
  Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController customerDisplayIdController = TextEditingController();

  login() {
    Get.to(ProductScreenCustomer());
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff4C4F55),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: SizedBox(
                width: 250,
                child: Row(
                  children: [
                    Expanded(
                      child: TextField(
                        decoration: InputDecoration(
                            labelText: "Enter Display ID",
                            labelStyle: TextStyle(color: Colors.white)),
                        controller: customerDisplayIdController,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    FloatingActionButton(
                      onPressed: login,
                      tooltip: 'Increment',
                      child: Icon(Icons.login),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
