class ClientMessageEvent {
  final String event;
  final String? room;
  final int? request_time;
  final int? owner;
  final Map<String, dynamic>? data;

  ClientMessageEvent(
      {required this.event,
      this.data,
      this.room,
      this.owner,
      this.request_time});

  factory ClientMessageEvent.fromJson(Map<String, dynamic> json) {
    return ClientMessageEvent(
      event: json['event'],
      data: json['data'],
      room: json['room'],
      owner: json['owner'],
      request_time: json['request_time'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'event': event,
      'data': data,
      'owner': owner,
      'room': room,
      'request_time': request_time,
    };
  }
}