import 'package:customer_display/config.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class PaymentScreen extends StatefulWidget {
  const PaymentScreen({
    Key? key,
    required this.cryptoQr,
    required this.tax,
    required this.total,
    required this.subTotal,
    required this.balance,
    required this.type,
    this.status,
    required this.gratuity,
    required this.surcharge,
    required this.itemDiscount,
    required this.discount,
  }) : super(key: key);

  final String cryptoQr;
  final String type;
  final double tax;
  final double total;
  final double subTotal;
  final String balance;
  final String? status;
  final String? gratuity;
  final String? surcharge;
  final double itemDiscount;
  final double discount;

  @override
  State<PaymentScreen> createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white.withAlpha(26), // 0.1 * 255 ≈ 26
      body: Center(
        child: AspectRatio(
          aspectRatio:
              0.7, // Adjust this ratio to match your display's aspect ratio
          child: FittedBox(
            fit: BoxFit.contain,
            child: SizedBox(
              width: 300, // Base width for scaling
              child: widget.type != 'crypto'
                  ? _buildRegularPayment()
                  : _buildCryptoPayment(),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildRegularPayment() {
    return Card(
      elevation: 4,
      margin: EdgeInsets.zero,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      child: Container(
        color: Colors.white.withAlpha(191), // 0.75 * 255 ≈ 191
        padding: const EdgeInsets.all(3),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              widget.status == 'Payment failed'
                  ? Icons.error_outline
                  : Icons.check_circle_outline,
              size: 48,
              color:
                  widget.status == 'Payment failed' ? Colors.red : Colors.green,
            ),
            const SizedBox(height: 4),
            Text(
              widget.type.toUpperCase(),
              style: TextStyle(
                color: Colors.black.withAlpha(179), // 0.7 * 255 ≈ 179
                fontSize: 24,
                fontWeight: FontWeight.w400,
              ),
            ),
            const SizedBox(height: 4),
            Text(
              widget.status == 'Payment failed'
                  ? 'Transaction Failed'
                  : 'Transaction Successful',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black.withAlpha(179), // 0.7 * 255 ≈ 179
                fontSize: 20,
                fontWeight: FontWeight.w300,
              ),
            ),
            if (widget.balance.isNotEmpty) ...[
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: Divider(height: 1),
              ),
              Text(
                widget.balance,
                style: TextStyle(
                  color: Colors.black.withAlpha(179), // 0.7 * 255 ≈ 179
                  fontSize: 18,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ],
            Container(
              margin: const EdgeInsets.only(top: 5),
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
              ),
              child: _buildPaymentDetails(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCryptoPayment() {
    return Card(
      elevation: 4,
      margin: EdgeInsets.zero,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: double.infinity,
            padding: const EdgeInsets.all(3),
            color: Colors.white,
            child: Text(
              'Pay with Crypto',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20,
                color: Colors.black.withAlpha(204), // 0.8 * 255 ≈ 204
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(3),
            color: Colors.white.withAlpha(191), // 0.75 * 255 ≈ 191
            child: AspectRatio(
              aspectRatio: 1,
              child: FadeInImage.memoryNetwork(
                image: widget.cryptoQr,
                placeholder: kTransparentImage,
                fit: BoxFit.contain,
              ),
            ),
          ),
          Container(
            width: double.infinity,
            padding: const EdgeInsets.all(12),
            color: Colors.white,
            child: _buildPaymentDetails(),
          ),
        ],
      ),
    );
  }

  Widget _buildPaymentDetails() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _buildDetailRow('Sub Total', widget.subTotal),
        _buildDetailRow('Item discount', widget.itemDiscount, isNegative: true),
        _buildDetailRow('Discount', widget.discount, isNegative: true),
        _buildDetailRow(
            'Surcharge', double.tryParse(widget.surcharge ?? "0") ?? 0),
        _buildDetailRow(
            'Gratuity', double.tryParse(widget.gratuity ?? "0") ?? 0),
        _buildDetailRow('Tax (Includes)', widget.tax),
        const SizedBox(height: 1),
        _buildDetailRow(
          'Total',
          widget.total,
          isTotal: true,
        ),
      ],
    );
  }

  Widget _buildDetailRow(String label, double amount,
      {bool isTotal = false, bool isNegative = false}) {
    final textStyle = TextStyle(
      color: Colors.black.withAlpha(204), // 0.8 * 255 ≈ 204
      fontFamily: 'Akkurat',
      fontSize: isTotal ? 14 : 12,
      fontWeight: isTotal ? FontWeight.bold : FontWeight.normal,
      height: 1.2,
    );

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(label, style: textStyle),
        if (isNegative)
          Text(
            '-${Config.currency}${amount.toStringAsFixed(2)}',
            style: textStyle,
          ),
        if (!isNegative)
          Text(
            '${Config.currency}${amount.toStringAsFixed(2)}',
            style: textStyle,
          ),
      ],
    );
  }
}