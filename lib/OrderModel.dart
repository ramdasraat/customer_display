// To parse this JSON data, do
//
//     final ticketObject = ticketObjectFromJson(jsonString);

import 'dart:convert';

List<TicketObject> ticketObjectFromJson(String str) => List<TicketObject>.from(
    json.decode(str).map((x) => TicketObject.fromJson(x)));

String ticketObjectToJson(List<TicketObject> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TicketObject {
  TicketObject(
      {this.id,
      this.count,
      this.voidItem,
      this.voidReason,
      this.kitchenNote,
      this.notePrice,
      this.rateOfDiscount,
      this.tempRate,
      this.menu,
      this.size,
      this.paidcount,
      this.tag,
      this.modifiers,
      this.combo_name,
      this.comboCombinedPrice,
      this.comboBatch,
      this.store_id,
      this.promo_name,
      this.promoId
      });

  String? id;
  double? count;
  int? voidItem;
  dynamic voidReason;
  String? kitchenNote;
  double? notePrice;
  double? rateOfDiscount;
  double? tempRate;
  Menu? menu;
  String? size;
  int? paidcount;
  String? combo_name;
  String? comboBatch;
  dynamic tag;
  List<Modifier>? modifiers;
  double? comboCombinedPrice;
  int ? store_id;
  String? promoId;
  String? promo_name;

  num get() {
    return (tempRate! * count!);
  }

  factory TicketObject.fromJson(Map<String, dynamic> json) => TicketObject(
        id: json["id"],
        count: json["count"].toDouble(),
        voidItem: json["voidItem"],
        voidReason: json["voidReason"],
        kitchenNote: json["kitchenNote"] == null ? null : json["kitchenNote"],
        notePrice: (json["notePrice"]??0).toDouble(),
        rateOfDiscount: json["rateOfDiscount"].toDouble(),
        tempRate: json["tempRate"].toDouble(),
        menu: Menu.fromJson(json["menu"]),
        size: json["size"] == null ? null : json["size"],
        paidcount: json["paidcount"],
        tag: json["tag"],
        store_id: json["store_id"],
        promoId: json["promoId"],
        combo_name: json["combo_name"],
        promo_name: json["promo_name"],
        comboBatch: json["combo_batch_id"]==""?null:json["combo_batch_id"],
        comboCombinedPrice: (json["combo_combined_price"] != null &&
                json["combo_combined_price"] != "")
            ? json["combo_combined_price"].toDouble()
            : 0.0,
        modifiers: List<Modifier>.from(
            json["modifiers"].map((x) => Modifier.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "count": count,
        "voidItem": voidItem,
        "voidReason": voidReason,
        "kitchenNote": kitchenNote == null ? null : kitchenNote,
        "notePrice": notePrice,
        "rateOfDiscount": rateOfDiscount,
        "tempRate": tempRate,
        "menu": menu!.toJson(),
        "size": size == null ? null : size,
        "paidcount": paidcount,
        "tag": tag,
        "modifiers": List<dynamic>.from(modifiers!.map((x) => x.toJson())),
        "combo_name": combo_name,
        "store_id": store_id,
      };
}

class Menu {
  Menu({
    this.id,
    this.categoryId,
    this.subcategoryId,
    this.userId,
    this.name,
    this.description,
    this.salePrice,
    this.photo,
    this.color,
    this.vatId,
    this.code,
    this.askprice,
    this.askqty,
    this.kitchennote,
    this.weight,
    this.discountable,
    this.cost,
    this.isDelete,
    this.createdAt,
    this.updatedAt,
    this.isInventory,
    this.kitchEnnote,
    this.cOlor,
    this.menuUserId,
    this.crEatedAt,
  });

  int? id;
  int? categoryId;
  int? subcategoryId;
  int? userId;
  String? name;
  String? description;
  double? salePrice;
  String? photo;
  String? color;
  String? vatId;
  String? code;
  int? askprice;
  int? askqty;
  int? kitchennote;
  int? weight;
  int? discountable;
  double? cost;
  int? isDelete;
  String? createdAt;
  String? updatedAt;
  int? isInventory;
  int? kitchEnnote;
  String? cOlor;
  int? menuUserId;
  String? crEatedAt;

  factory Menu.fromJson(Map<String, dynamic> json) => Menu(
        id: json["id"],
        categoryId: json["category_id"],
        subcategoryId: json["subcategory_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        name: json["name"],
        description: json["description"],
        salePrice: json["sale_price"].toDouble(),
        photo: json["photo"],
        color: json["color"] == null ? null : json["color"],
        vatId: json["vat_id"],
        code: json["code"],
        askprice: json["askprice"],
        askqty: json["askqty"],
        kitchennote: json["kitchennote"] == null ? null : json["kitchennote"],
        weight: json["weight"],
        discountable: json["discountable"],
        cost: json["cost"].toDouble(),
        isDelete: json["is_delete"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"],
        isInventory: json["is_inventory"],
        kitchEnnote: json["kitch ennote"] == null ? null : json["kitch ennote"],
        cOlor: json["c olor"] == null ? null : json["c olor"],
        menuUserId: json["user_id "] == null ? null : json["user_id "],
        crEatedAt: json["cr eated_at"] == null ? null : json["cr eated_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "category_id": categoryId,
        "subcategory_id": subcategoryId,
        "user_id": userId == null ? null : userId,
        "name": name,
        "description": description,
        "sale_price": salePrice,
        "photo": photo,
        "color": color == null ? null : color,
        "vat_id": vatId,
        "code": code,
        "askprice": askprice,
        "askqty": askqty,
        "kitchennote": kitchennote == null ? null : kitchennote,
        "weight": weight,
        "discountable": discountable,
        "cost": cost,
        "is_delete": isDelete,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt,
        "is_inventory": isInventory,
        "kitch ennote": kitchEnnote == null ? null : kitchEnnote,
        "c olor": cOlor == null ? null : cOlor,
        "user_id ": menuUserId == null ? null : menuUserId,
        "cr eated_at": crEatedAt == null ? null : crEatedAt,
      };
}

class Modifier {
  Modifier({
    this.id,
    this.quantity,
    this.name,
    this.price,
  });

  int? id;
  int? quantity;
  String? name;
  double? price;

  factory Modifier.fromJson(Map<String, dynamic> json) => Modifier(
        id: json["id"],
        quantity: json["quantity"],
        name: json["name"],
        price: json["price"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "quantity": quantity,
        "name": name,
        "price": price,
      };
}

class TagClass {
  TagClass({
    this.id,
    this.userId,
    this.tagName,
    this.tagPrice,
    this.productId,
    this.isDelete,
    this.creatEdAt,
    this.updatedAt,
    this.createdAt,
    this.isDeleTe,
  });

  int? id;
  int? userId;
  String? tagName;
  int? tagPrice;
  int? productId;
  int? isDelete;
  String? creatEdAt;
  String? updatedAt;
  String? createdAt;
  int? isDeleTe;

  factory TagClass.fromJson(Map<String, dynamic> json) => TagClass(
        id: json["id"],
        userId: json["user_id"],
        tagName: json["tag_name"],
        tagPrice: json["tag_price"],
        productId: json["product_id"],
        isDelete: json["is_delete"] == null ? null : json["is_delete"],
        creatEdAt: json["creat ed_at"] == null ? null : json["creat ed_at"],
        updatedAt: json["updated_at"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        isDeleTe: json["is_dele te"] == null ? null : json["is_dele te"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "tag_name": tagName,
        "tag_price": tagPrice,
        "product_id": productId,
        "is_delete": isDelete == null ? null : isDelete,
        "creat ed_at": creatEdAt == null ? null : creatEdAt,
        "updated_at": updatedAt,
        "created_at": createdAt == null ? null : createdAt,
        "is_dele te": isDeleTe == null ? null : isDeleTe,
      };
}

class CustomerObject {
  CustomerObject(
      {this.id,
      this.customerId,
      this.userId,
      this.terminalId,
      this.tel,
      this.name,
      this.addressLine1,
      this.addressLine2,
      this.zipCode,
      this.email,
      this.isDelete,
      this.createdAt,
      this.updatedAt,
      this.credit,
      this.loyality,
      this.isSync});

  int? id;
  String? customerId;
  int? userId;
  int? terminalId;
  String? tel;
  String? name;
  String? addressLine1;
  String? addressLine2;
  String? zipCode;
  String? email;
  int? isDelete;
  String? createdAt;
  String? updatedAt;
  double? credit;
  int? loyality;
  int? isSync = 0;

  CustomerObject copyWith(
          {int? id,
          String? customerId,
          int? userId,
          int? terminalId,
          String? tel,
          String? name,
          String? addressLine1,
          String? addressLine2,
          String? zipCode,
          String? email,
          int? isDelete,
          String? createdAt,
          String? updatedAt,
          double? credit,
          int? loyality,
          int? isSync}) =>
      CustomerObject(
          id: id ?? this.id,
          customerId: customerId ?? this.customerId,
          userId: userId ?? this.userId,
          terminalId: terminalId ?? this.terminalId,
          tel: tel ?? this.tel,
          name: name ?? this.name,
          addressLine1: addressLine1 ?? this.addressLine1,
          addressLine2: addressLine2 ?? this.addressLine2,
          zipCode: zipCode ?? this.zipCode,
          email: email ?? this.email,
          isDelete: isDelete ?? this.isDelete,
          createdAt: createdAt ?? this.createdAt,
          updatedAt: updatedAt ?? this.updatedAt,
          credit: credit ?? this.credit,
          loyality: loyality ?? this.loyality,
          isSync: isSync ?? this.isSync);

  factory CustomerObject.fromJson(Map<String, dynamic> json) => CustomerObject(
      id: json["id"],
      customerId: json["customer_id"],
      userId: json["user_id"],
      terminalId: json["terminal_id"],
      tel: json["tel"],
      name: json["name"],
      addressLine1: json["addressLine1"],
      addressLine2: json["addressLine2"],
      zipCode: json["zipCode"],
      email: json["email"],
      isDelete: json["is_delete"],
      createdAt: json["created_at"],
      updatedAt: json["updated_at"],
      credit: json["credit"].toDouble(),
      loyality: json["loyality"],
      isSync: json['is_sync']);

  Map<String, dynamic> toJson() => {
        "id": id,
        "customer_id": customerId,
        "user_id": userId,
        "terminal_id": terminalId,
        "tel": tel,
        "name": name,
        "addressLine1": addressLine1,
        "addressLine2": addressLine2,
        "zipCode": zipCode,
        "email": email,
        "is_delete": isDelete,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "credit": credit,
        "loyality": loyality,
        "is_sync": isSync
      };
}
